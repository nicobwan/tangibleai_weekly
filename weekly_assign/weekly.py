#Turn csv into DataFrame
# import pandas module 
import pandas as pd 
    
# making dataframe 
fake = pd.read_csv("Fake.csv") 
true = pd.read_csv("True.csv")
# output the dataframe
# print(fake)


#combine the two dataframes
# combined = pd.concat([fake, true], axis=1)
#rowise
combined = pd.concat([fake,true], ignore_index=True)
# print(combined)
# add a new column
combined['real_or_not'] = 0
# change to csv
combined.to_csv('combined.csv',index=False)

